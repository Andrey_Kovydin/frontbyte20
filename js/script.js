$(document).ready(function() {
    $('.header__burger').click(function(event) {
        $('.header__burger, .b-header__lang, .b-header__menu,' +
            ' .b-subtitle--header-mobile, .b-header-title-mobile,' +
            ' .b-header__logo, .b-header-container, .b-button--header').toggleClass('active');
        $('.b-header__logo-icon--mobile-dark, .b-header__logo-icon--mobile-light').toggleClass('active');
    });
});